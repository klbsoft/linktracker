(function () {
    'use strict';

    angular
        .module('mainApp')
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', 'MainService'];

    function MainController($scope, MainService) {
        /*
        * For all links on page
        * */
        angular.element('a')
            .on('click', captureClick)
            .on('mouseover', captureMouseOver);

        /*
        * For all links in div with id 'footer-panel'
        * */
        angular.element('#footer-panel a')
            .on('click', captureClickFooterPanel);

        /*
        *
        * Capture click event
        *
        * */
        function captureClick (event, data) {
            // first of all, disable link transition
            event.preventDefault();
        }

        /*
        * Send data by using service
        * */
        function setSomeData(data) {
            MainService.setSomeData(data).then(getSomeData)
        }

        /*
        * Get some data by using service
        * */
        function getSomeData(data) {
            console.log(data);
        }

        /*
         *
         * Capture mouse over event
         *
         * */
        function captureMouseOver ( event, data) {
            // do something
            var innerText = event.target.innerText;

            setSomeData({innerText: innerText});
        }

        function captureClickFooterPanel(event, data) {
            // first of all, disable link transition
            event.preventDefault();

            console.log('another click')

        }
    }
})();