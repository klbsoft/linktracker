(function(){
    'use strict';

    angular
        .module('mainApp')
        .factory('MainService', MainService);

    MainService.$inject = ['$http', '$q'];
    function MainService($http, $q){
        return {
            getSomeData: function() {
                var deferred = $q.defer();

                var conf = {
                    method: 'GET',
                    url: '/api/'
                };

                $http(conf)
                    .success(function(data, status, headers, config) {
                        deferred.resolve(data);
                    })
                    .error(function(data, status, headers, config) {
                        deferred.reject(status);
                    });
                return deferred.promise;
            },
            setSomeData: function(data){
                var deferred = $q.defer();

                var conf = {
                    method: 'POST',
                    url: '/api/',
                    data: data
                };

                $http(conf)
                    .success(function(data, status, headers, config) {
                        deferred.resolve(data);
                    })
                    .error(function(data, status, headers, config) {
                        deferred.reject(status);
                    });
                return deferred.promise;
            }
        }
    }

})();